{%- for authority, metadata in salt['pillar.get']('pki:crl', {}).iteritems() %}

ensure crl directory exists for authority {{ authority }}:
  file.directory:
    - name: {{ metadata.path }}

create crl from list of certificates for authority {{ authority }}:
  x509.crl_managed:
    - name: {{ metadata.path }}/{{ metadata.name }}
    - signing_private_key: {{ metadata.private_key }}
    - signing_cert: {{ metadata.signing_cert }}
    - digest: {{ metadata.digest }}
    - revoked:
      {%- for certificate, options in metadata.get('revoked', {}).items() %}
      - {{ certificate }}:
        {%- for key, value in options.items() %}
        - {{ key }}: {{ value }}
        {%- endfor %}
      {%- endfor %}
{%- endfor %}