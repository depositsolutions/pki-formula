{# Salt state to sign CSR for client certificates #}

client-certificate-directory:
  file.directory:
    - name: /srv/pki/clients

client-certificate-copy-directory:
  file.directory:
    - name: /srv/pki/clients/issued_certs

{% for client, properties in salt['pillar.get']('pki:clients', {}).iteritems() %}
{% if not salt['file.file_exists']('/srv/pki/clients/{{client}}.csr') %}
csr-{{client}}:
  file.managed:
    - name: /srv/pki/clients/{{client}}.csr
    - contents: |
        {{ properties.csr | indent(8) }}
{% endif %}

{#- Default signing policy will be the normal 'client' one #}
{%- set policy = properties.get('policy', 'client') %}

client-cert-{{client}}:
  x509.certificate_managed:
    - name: /srv/pki/clients/{{client}}.client.cert.pem
    - csr: /srv/pki/clients/{{client}}.csr
    - signing_policy: {{ policy }}
    {#- The following attributes must be specified here to work #}
    - CN: {{ client }}
    {#- By default renew a client certificate 3 days before it expires #}
    - days_remaining: {{ properties.get('days_remaining', 3) }}
    {#- The following attributes are retrieved from the pillar #}
    {%- if properties.get('options') %}
    {%- for property, value in properties.get('options').items() %}
    - {{ property }}: {{ value }}
    {%- endfor %}
    {%- endif %}
    - require:
      - file: csr-{{client}}

{%- set default_admin_contact = salt['pillar.get']('pki:defaults:admin_contact') %}

{%- set policy_options_list = salt['config.get']('x509_signing_policies').get(policy) %}
{%- set policy_options = {} %}

{%- for option in policy_options_list %}
{%- do policy_options.update({option.keys()[0]: option.values()[0]}) %}
{%- endfor %}

{# Mail client certificates according to the policy. Collect the recipients and
then if there are actually recipients, send the message out #}
{%- set recipients = {} %}

{%- if policy_options.get('email_client', False) == True %}
{# Add either the CN or the client_contact email to the recipient list #}
{%- set client_contact = properties.get('client_contact', client) %}
{%- do recipients.update({client_contact: ''}) %}
{%- endif %}

{%- if policy_options.get('email_admin', False) == True %}
{# Add the admin contact to the recipient list #}
{%- set admin_contact = properties.get('admin_contact', default_admin_contact) %}
{%- do recipients.update({admin_contact: ''}) %}
{%- endif %}

mail client certificate for {{ client }}:
  smtp.send_msg:
    - name: |
       Client certificate issued for {{ client }} and attached to this message.
    - subject: 'PKI: certificate issued for {{ client }}'
    - recipient: {{ recipients.keys() | join(',') }}
    - profile: dc-smtp
    - attachments:
      - '/srv/pki/clients/{{client}}.client.cert.pem'
    - onchanges:
      - x509: client-cert-{{ client }}

{# End for-clients #}
{%- endfor %}

schedule client certificate renewal job:
  schedule.present:
    - function: state.sls
    - job_args:
      - pki.clients
    - cron: {{ salt['pillar.get']('pki:clients:cron', '5 8 * * *') }}
