{# Salt state to sign CSR for managed server certificates #}

server-certificate-directory:
  file.directory:
    - name: /srv/pki/servers

server-certificate-copy-directory:
  file.directory:
    - name: /srv/pki/servers/issued_certs

{% for server, properties in salt['pillar.get']('pki:servers', {}).iteritems() %}
{% if not salt['file.file_exists']('/srv/pki/servers/{{server}}.csr') %}
csr-{{server}}:
  file.managed:
    - name: /srv/pki/servers/{{server}}.csr
    - contents: |
        {{ properties.csr | indent(8) }}
{% endif %}

server-cert-{{server}}:
  x509.certificate_managed:
    - name: /srv/pki/servers/{{server}}.pem
    - csr: /srv/pki/servers/{{server}}.csr
    {#- Default signing policy will be the normal 'client' one #}
    - signing_policy: {{ properties.get('policy', 'server-managed-blank') }}
    {#- The following attributes must be specified here to work #}
    # TODO: enable default attributes depending on policy.
    # - CN: {{ server }}
    {#- The following attributes are retrieved from the pillar #}
    {%- if properties.get('attributes') %}
    {%- for property, value in properties.get('attributes').items() %}
    {%- if property == 'subjectAltName' %}
    {%- set names = [] %}
    {%- for label, name in value.items() %}
    {%- do names.append(name) %}
    {%- endfor %}
    - {{ property }}: "{{ names | join(', ') }}"
    {%- else %}
    - {{ property }}: "{{ value | join(', ') }}"
    {%- endif %}
    {%- endfor %}
    {%- endif %}
    - require:
      - file: csr-{{server}}