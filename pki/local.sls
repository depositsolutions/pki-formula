{%- if 'Amazon' not in grains['osfinger'] %}

include:
  - pki

{% from "pki/map.jinja" import pki with context %}

{%- set rootca_minion = salt['pillar.get']('pki:rootca:minion') %}
{%- set intermediateca_minion = salt['pillar.get']('pki:intermediateca:minion') %}
{%- set rootca = salt['mine.get'](rootca_minion, 'x509.get_pem_entries')[rootca_minion]['/srv/pki/rootca.pem'] %}
{%- set intermediateca = salt['mine.get'](intermediateca_minion, 'x509.get_pem_entries')[intermediateca_minion]['/srv/pki/intermediateca.pem'] %}

{# Copy of Root CA #}
/srv/certs/rootca.pem:
  x509.pem_managed:
    - text: {{ rootca | replace('\n', '') }}

{# Copy of Intermediate CA #}
/srv/certs/intermediateca.pem:
  x509.pem_managed:
    - text: {{ intermediateca | replace('\n', '') }}

{# Bundle certificate #}
/srv/certs/bundleca.pem:
  file.managed:
    - contents: |
        {{ (rootca + intermediateca) | indent(8) }}
    - require:
      - x509: /srv/certs/intermediateca.pem
      - x509: /srv/certs/rootca.pem

{% if grains['os'] == 'CentOS' %}
remove old bundleca from local machine trust:
  file.absent:
    - name: /etc/pki/ca-trust/source/anchors/bundleca.pem

trust root ca on centos in local trust:
  file.symlink:
    - name: /etc/pki/ca-trust/source/anchors/ds-rootca.pem
    - target: /srv/certs/rootca.pem
    - force: True
  cmd.run:
    - name: update-ca-trust
    - unless: openssl verify /srv/certs/rootca.pem
    - require:
      - x509: /srv/certs/rootca.pem
      - file: remove old bundleca from local machine trust

trust intermediate ca on centos in local trust:
  file.symlink:
    - name: /etc/pki/ca-trust/source/anchors/ds-intermediateca.pem
    - target: /srv/certs/intermediateca.pem
    - force: True
  cmd.run:
    - name: update-ca-trust
    - unless: openssl verify /srv/certs/intermediateca.pem
    - require:
      - x509: /srv/certs/intermediateca.pem
      - file: remove old bundleca from local machine trust
{% endif %}

{% if grains['os'] == 'Debian' %}
{# Trust bundle certificate in the system #}
remove old bundleca from local machine trust:
  file.absent:
    - name: /etc/ssl/certs/bundleca.pem

trust root ca on debian in local trust:
  file.symlink:
    - name: /usr/local/share/ca-certificates/ds-rootca.crt
    - target: /srv/certs/rootca.pem
    - force: True
  cmd.run:
    - name: update-ca-certificates
    - unless: openssl verify /srv/certs/rootca.pem
    - require:
      - x509: /srv/certs/rootca.pem
      - file: remove old bundleca from local machine trust

trust intermediate ca on debian in local trust:
  file.symlink:
    - name: /usr/local/share/ca-certificates/ds-intermediateca.crt
    - target: /srv/certs/intermediateca.pem
    - force: True
  cmd.run:
    - name: update-ca-certificates
    - unless: openssl verify /srv/certs/intermediateca.pem
    - require:
      - x509: /srv/certs/intermediateca.pem
      - file: remove old bundleca from local machine trust
{% endif %}

{# Local machine key #}
/srv/certs/{{ grains['id'] }}.key:
  x509.private_key_managed:
    - bits: 4096
    - backup: True
    - new: True
    {% if salt['file.file_exists']('/srv/certs/' + grains['id'] + '.key') %}
    - prereq:
      - x509: /srv/certs/{{ grains['id'] }}.pem
    {% endif %}
    - require:
      - pkg: {{ pki.runtime.m2crypto_package }}

create alias for local machine key:
  file.symlink:
    - name: /srv/certs/machine.key
    - target: /srv/certs/{{ grains['id'] }}.key
    - force: True
    - require:
      - x509: /srv/certs/{{ grains['id'] }}.key

{# Local machine certificate - server #}
/srv/certs/{{ grains['id'] }}.pem:
  x509.certificate_managed:
    - ca_server: {{ intermediateca_minion }}
    - signing_policy: server
    - public_key: /srv/certs/{{ grains['id'] }}.key
    - CN: {{ grains['id'] }}
    - days_remaining: 30
    - backup: True
    {%- for entry, value in salt['pillar.get']('pki:local:attributes', {}).iteritems() %}
    {%- if entry == 'subjectAltName' %}
    {%- set names = [] %}
    {%- for label, name in value.items() %}
    {%- do names.append(name) %}
    {%- endfor %}
    - {{ entry }}: "{{ names | join(', ') }}"
    {%- else %}
    - {{ entry }}: "{{ value | join(', ') }}"
    {%- endif %}
    {%- endfor %}
    - require:
      - pkg: {{ pki.runtime.m2crypto_package }}

append intermediate to server cert:
  file.append:
    - name: /srv/certs/{{ grains['id'] }}.pem
    - source: /srv/certs/intermediateca.pem

create alias for local machine certificate:
  file.symlink:
    - name: /srv/certs/machine.pem
    - target: /srv/certs/{{ grains['id'] }}.pem
    - force: True
    - require:
      - x509: /srv/certs/{{ grains['id'] }}.pem

{# Local machine certificate - client #}
/srv/certs/{{ grains['id'] }}.client.pem:
  x509.certificate_managed:
    - ca_server: {{ intermediateca_minion }}
    - signing_policy: machine-client
    - public_key: /srv/certs/{{ grains['id'] }}.key
    - CN: {{ grains['id'] }}
    - days_remaining: 30
    - backup: True
    {%- for entry, value in salt['pillar.get']('pki:local:attributes', {}).iteritems() %}
    {%- if entry == 'subjectAltName' %}
    {%- set names = [] %}
    {%- for label, name in value.items() %}
    {%- do names.append(name) %}
    {%- endfor %}
    - {{ entry }}: "{{ names | join(', ') }}"
    {%- else %}
    - {{ entry }}: "{{ value | join(', ') }}"
    {%- endif %}
    {%- endfor %}
    - require:
      - pkg: {{ pki.runtime.m2crypto_package }}

create alias for local machine client certificate:
  file.symlink:
    - name: /srv/certs/machine.client.pem
    - target: /srv/certs/{{ grains['id'] }}.client.pem
    - force: True
    - require:
      - x509: /srv/certs/{{ grains['id'] }}.client.pem

{# Local machine server certificate bundle #}
/srv/certs/{{ grains['id'] }}.bundle.pem:
  cmd.run:
    - name: 'cat /srv/certs/{{ grains['id']}}.key /srv/certs/{{ grains['id'] }}.pem > /srv/certs/{{ grains['id'] }}.bundle.pem'
    {# If the file was already created, then change it only when the source files change #}
    {%- if salt['file.file_exists']('/srv/certs/' + grains['id'] + '.bundle.pem') %}
    - onchanges:
      - x509: /srv/certs/{{ grains['id'] }}.key
      - x509: /srv/certs/{{ grains['id'] }}.pem
    {%- endif %}
  file.managed:
    - user: root
    - group: ssl-cert
    - mode: 0640
    - require:
      - cmd: /srv/certs/{{ grains['id'] }}.bundle.pem

create alias for local machine certificate bundle:
  file.symlink:
    - name: /srv/certs/machine.bundle.pem
    - target: /srv/certs/{{ grains['id'] }}.bundle.pem
    - force: True
    - require:
      - cmd: /srv/certs/{{ grains['id'] }}.bundle.pem

{# Local machine client certificate bundle #}
/srv/certs/{{ grains['id'] }}.client.bundle.pem:
  cmd.run:
    - name: 'cat /srv/certs/{{ grains['id']}}.key /srv/certs/{{ grains['id'] }}.client.pem > /srv/certs/{{ grains['id'] }}.client.bundle.pem'
    {# If the file was already created, then change it only when the source files change #}
    {%- if salt['file.file_exists']('/srv/certs/' + grains['id'] + '.client.bundle.pem') %}
    - onchanges:
      - x509: /srv/certs/{{ grains['id'] }}.key
      - x509: /srv/certs/{{ grains['id'] }}.client.pem
    {%- endif %}
  file.managed:
    - user: root
    - group: ssl-cert
    - mode: 0640
    - require:
      - cmd: /srv/certs/{{ grains['id'] }}.client.bundle.pem

create alias for local machine client certificate bundle:
  file.symlink:
    - name: /srv/certs/machine.client.bundle.pem
    - target: /srv/certs/{{ grains['id'] }}.client.bundle.pem
    - force: True
    - require:
      - cmd: /srv/certs/{{ grains['id'] }}.client.bundle.pem

fix permissions for key:
  file.managed:
    - name: /srv/certs/{{ grains['id'] }}.key
    - user: root
    - group: ssl-cert
    - mode: 0640
    - require:
      - x509: /srv/certs/{{ grains['id'] }}.key

{% endif %} # Ending check for amazon
