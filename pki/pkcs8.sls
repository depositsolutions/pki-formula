{# Add a version of the current host key in the PKCS8 format #}
include:
  - pki.local

issue PKCS8 key for {{ grains['id'] }}:
  cmd.run:
    - names:
      - openssl pkcs8 -topk8 -inform PEM -outform PEM -in /srv/certs/{{ grains['id'] }}.key -out /srv/certs/{{ grains['id'] }}.pkcs8.key -nocrypt
    - require:
      - x509: /srv/certs/{{ grains['id'] }}.key

fix permissions for pkcs8 key:
  file.managed:
    - name: /srv/certs/{{ grains['id'] }}.pkcs8.key
    - user: root
    - group: ssl-cert
    - mode: 0640
    - require:
      - cmd: issue PKCS8 key for {{ grains['id'] }}

create alias for local machine pkcs8 key:
  file.symlink:
    - name: /srv/certs/machine.pkcs8.key
    - target: /srv/certs/{{ grains['id'] }}.pkcs8.key
    - force: True
    - require:
      - file: fix permissions for pkcs8 key
