{# Import main configuration #}
{% from "pki/map.jinja" import pki with context %}

{# Need to install support for encrypting #}
install-python-crypto:
  pkg.installed:
    - name: {{ pki.runtime.m2crypto_package }}

{# Install the ssl-cert package if the distro has it available #}
{% if pki.runtime.ssl_cert %}
install-ssl_cert-package:
  pkg.installed:
    - name: ssl-cert
{% endif %}

{# Make sure that we have the ssl-cert group present in the machine #}
ensure group ssl-cert is present:
  group.present:
    - name: ssl-cert
    - system: True
  {% if pki.runtime.ssl_cert %}
    - require:
      - pkg: install-ssl_cert-package
  {% endif %}

{# Create the base directory to host all certificates #}
ensure certificates directory is present:
  file.directory:
    - name: /srv/certs
    - user: root
    - group: root
    - mode: 775
