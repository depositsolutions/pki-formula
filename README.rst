PKI Formula
===========

This formula is used to manage a PKI structure completely with Salt. It
is used to issue a Root CA, followed by an intermediate CA which is then
used as a CA from individual server certificates that can be used to
protect services.

Available states
----------------

.. contents::
    :local:

``clients``
-----------

Issue client certificates for CSR stored under the ``pki:clients`` pillar. This 
state should be assigned to all machines hosting certificate authorities that sign
client certificates.

``crl``
-------

Issue a CRL list for all authorities with the revoked certificates defined
under the ``pki:crl`` pillar. This state should be assigned to all machines 
hosting certificate authorities.

``init``
--------

Install the basic packages required for the PKI system to work. This will be 
automatically included by all dependent states.

``intermediate``
----------------

Setup a key and an intermediate certificate authority. The public part of 
the authority will be published to the Salt Mine.

``local``
---------

Issue local server certificates signed by an Intermediate CA authority. This state
should be assigned to all machines that require server certificates.

``pkcs8``
---------

Convert a local key to the PKCS8 format. This is required by some Java applications.


``root``
--------

Setup a key and a root certificate authority. The public part of 
the authority will be published to the Salt Mine.

``servers``
-----------

Analog to the ``clients`` state, this state issues server certificates for CSR
stored under the ``pki:servers ``pillar. This makes it possible to sign certificates
for 3rd party appliances using the certificate authorities managed by Salt.

Bootstrapping
-------------

The bootstrapping process consists in creating a *Root CA* and then
issuing an *Intermediate CA* signed by it.

The Root CA needs to be created first at the machine hosting it by using
the state ``pki.root``. This state needs to be assigned to the
machine that will be chosen as the Root Certificate Manager.

Then the Intermediate CA needs to be issued with the state
``pki.intermediate``. This state needs to be assigned to the
machine designated as the Intermediate Certificate Manager.

After both Root CA and Intermediate CA as issued, they will also be
published to the Salt Mine.

The following command can be used to verify whether the certificates are
correctly published to the Salt mine:

.. code:: ShellSession

    salt \* mine.get \* x509.get_pem_entries

Configuration
-------------

Some bits of configuration are required to make the system work.

Salt Master
~~~~~~~~~~~

The Salt Master should allow any minions (``*``) to call the
``x509.sign_remote_certificate`` function from any other minion. This
require the master configuration to have the following bit of config:

.. code:: yaml

    peer:                                                                      
      .*:                                                                      
        - x509.sign_remote_certificate 

Salt Minion
~~~~~~~~~~~

The Salt minions for the Root CA and Intermediate CA require the signing
policies to be deployed onto their respective minions.

Root CA
^^^^^^^

The Root CA minion should be responsible for holding the policies
required for issuing intermediate certificates, as below:

.. code:: yaml

    x509_signing_policies:
      intermediate:
        # List of minions allowed to use this policy
        - minions: 'intermediateca.minion.id'
        - signing_private_key: /srv/pki/rootca.key
        - signing_cert: /srv/pki/rootca.pem
        - C: DE
        - ST: Hamburg
        - L: Hamburg
        - O: My Org
        - OU: Infrastructure
        - basicConstraints: "critical CA:true, pathlen:0"
        - keyUsage: "critical cRLSign, keyCertSign"
        - emailAddress: admin@org.com
        - nsComment: "Issued by Salt"
        - crlDistributionPoints: "URI:http://pki.internal/root.crl.pem,URI:http://pki.org.com/root.crl.pem"
        - days_valid: 1825 
        - subjectKeyIdentifier: hash
        - authorityKeyIdentifier: keyid,issuer:always
        - copypath: /srv/pki/issued_certs/

Since the Root CA cannot be managed with minion policies, the Root CA
minion also needs some bit of config to be deployed into the
``pki:rootca`` pillar:

.. code:: yaml

    pki:
      defaults:
        admin_contact: admin@org.com
      rootca:
        options:
          CN: Org Root CA
          C: DE
          ST: Hamburg
          L: Hamburg
          O: My Org
          OU: Infrastructure
          emailAddress: admin@org.com
        - crlDistributionPoints: "URI:http://pki.internal/root.crl.pem,URI:http://pki.org.com/root.crl.pem"
          backup: True

Intermediate CA
^^^^^^^^^^^^^^^

The Intermediate CA minion is responsible for signing all other
certificates (machine certificates, client certificates, etc). To enable
this, the following policies need to be deployed in the Salt Minion
responsible for the Intermediate CA:

.. code:: yaml

    x509_signing_policies:
      # Default policy for server certificates. 
      # Here we allow a certificate to be used both as a server and 
      # machine-client certificate.
      server:
        # List of minions allowed to use this policy
        - minions: '*'
        - signing_private_key: /srv/pki/intermediateca.key
        - signing_cert: /srv/pki/intermediateca.pem
        - C: DE
        - ST: Hamburg
        - L: Hamburg
        - O: My Org
        - OU: Infrastructure
        - basicConstraints: "critical CA:false"
        - keyUsage: "critical digitalSignature, keyEncipherment"
        - extendedKeyUsage: "critical serverAuth, clientAuth"
        - nsCertType: server
        - emailAddress: admin@org.com
        - nsComment: "Issued by Salt"
        - crlDistributionPoints: "URI:http://pki.internal/intermediate.crl.pem,URI:http://pki.org.com/intermediate.crl.pem"
        - days_valid: 365
        - subjectKeyIdentifier: hash
        - authorityKeyIdentifier: keyid,issuer:always
        - copypath: /srv/pki/intermediate_issued_certs/
      # Policy for managed server certificates that should not carry any other
      # issuing information such as O, L, etc.
      server-managed-blank:
        # List of minions allowed to use this policy
        - minions: 'intermediateca.minion.id'
        - signing_private_key: /srv/pki/intermediateca.key
        - signing_cert: /srv/pki/intermediateca.pem
        - basicConstraints: "critical CA:false"
        - keyUsage: "critical digitalSignature, keyEncipherment"
        - extendedKeyUsage: "critical serverAuth, clientAuth"
        - nsCertType: server
        - emailAddress: admin@org.com
        - nsComment: "Issued by Salt"
        - crlDistributionPoints: "URI:http://pki.internal/intermediate.crl.pem,URI:http://pki.org.com/intermediate.crl.pem"
        - days_valid: 365
        - subjectKeyIdentifier: hash
        - authorityKeyIdentifier: keyid,issuer:always
        - copypath: /srv/pki/servers/issued_certs/
      # Default policy for a machine client certificate. 
      machine-client:
        # List of minions allowed to use this policy
        - minions: 'intermediateca.minion.id'
        - signing_private_key: /srv/pki/intermediateca.key
        - signing_cert: /srv/pki/intermediateca.pem
        - C: DE
        - ST: Hamburg
        - L: Hamburg
        - O: My Org
        - OU: Infrastructure
        - basicConstraints: "critical CA:false"
        - keyUsage: "critical digitalSignature, keyEncipherment"
        - extendedKeyUsage: "critical clientAuth"
        - nsCertType: client
        - emailAddress: admin@org.com
        - nsComment: "Issued by Salt"
        - crlDistributionPoints: "URI:http://pki.internal/intermediate.crl.pem,URI:http://pki.org.com/intermediate.crl.pem"
        - days_valid: 365
        - subjectKeyIdentifier: hash
        - authorityKeyIdentifier: keyid,issuer:always
        - copypath: /srv/pki/intermediate_issued_certs/
        - nsCertType: client
      # Default policy for a client certificate. We omit the validity and CN as
      # this should be filled in in the state when processing the certificate.
      # Only the certificate management minion can issue these certificates.
      # As they are internal certificates, we email them directly to the
      # recipients as soon as they are issued, but we also mail a copy to
      # ourselves for safekeeping.
      client:
        # List of minions allowed to use this policy
        - minions: 'intermediateca.minion.id'
        - signing_private_key: /srv/pki/intermediateca.key
        - signing_cert: /srv/pki/intermediateca.pem
        - C: DE
        - ST: Hamburg
        - L: Hamburg
        - O: My Org
        - OU: Infrastructure
        - basicConstraints: "critical CA:false"
        - keyUsage: "critical digitalSignature, keyEncipherment"
        - extendedKeyUsage: "critical clientAuth"
        - nsCertType: client
        - emailAddress: admin@org.com
        - nsComment: "Issued by Salt"
        - crlDistributionPoints: "URI:http://pki.internal/intermediate.crl.pem,URI:http://pki.org.com/intermediate.crl.pem"
        - subjectKeyIdentifier: hash
        - authorityKeyIdentifier: keyid,issuer:always
        - copypath: /srv/pki/clients/issued_certs/
        - days_valid: 90
        - backup: True
        - nsCertType: client
        - email_client: True
        - email_admin: True
        - admin_contact: admin@org.com
      # Default policy for an external client certificate. We let these clients
      # last a little longer so the clients have enough time to use them. We
      # also email them only to ourselves so we can forward the email to the
      # clients in a more controlled way.
      external-client:
        # List of minions allowed to use this policy
        - minions: 'intermediateca.minion.id'
        - signing_private_key: /srv/pki/intermediateca.key
        - signing_cert: /srv/pki/intermediateca.pem
        - basicConstraints: "critical CA:false"
        - keyUsage: "critical digitalSignature, keyEncipherment"
        - extendedKeyUsage: "critical clientAuth"
        - nsCertType: client
        - nsComment: "Issued by Salt"
        - crlDistributionPoints: "URI:http://pki.internal/intermediate.crl.pem,URI:http://pki.org.com/intermediate.crl.pem"
        - subjectKeyIdentifier: hash
        - authorityKeyIdentifier: keyid,issuer:always
        - copypath: /srv/pki/clients/issued_certs/
        - days_valid: 365
        - backup: True
        - nsCertType: client
        - email_client: False
        - email_admin: True
        - admin_contact: admin@org.com

Default configuration for attributes such as the ``admin_contact`` can
be specified by passing the following ``pki:defaults`` pillar to the
Intermediate CA machine:

.. code:: yaml

    pki:
      defaults:
        admin_contact: admin@org.com

All minions
~~~~~~~~~~~

In order to retrieve published certificates, all minions should receive
a basic set of configuration instructing from where they should fetch
the public parts for the Intermediate CA and Root CA. They also should,
for convenience, have a small bit of config instructing the DNS and IP
addresses from the machine that should be inside the certificate. This
configuration should be published not in the minion config, but under
the ``pki`` pillar instead, as below:

.. code:: yaml

    pki:
      rootca:
        minion: rootca.minion.id
      intermediateca:
        minion: intermediateca.minion.id
      local:
        attributes:
          subjectAltName:
            fqdn_dns:
              DNS:{{ grains['id'] }}
            {%- for ip in grains['fqdn_ip4'] %}
            ip_{{ ip }}:
              IP:{{ ip }}
            {%- endfor %}

Client certificates
-------------------

Client certificates can be issued against our *Intermediate CA* by using
the state ``pki.clients``.

These certificates are read from the pillar *pki:clients*. Each entry in
the pillar is an email that will be used as a CN for the certificate.
Under each entry, the following fields are required:

-  csr: the actual CSR for the certificate issued.
-  options: options to be passed to the *x509.certificate\_managed*
   state.

*Client certificates will, by default, be valid for 90 days and will be
renewed 3 days before the expiration date*.

Client certificates will always be saved under */srv/pki/clients*.

An example pillar for a client certificate would be:

.. code:: yaml

    pki:
      clients:
        firstname.lastname@provider.com:
          csr: |
            -----BEGIN CERTIFICATE REQUEST-----
            MIIEcDCCAlgCAQAwKzEpMCcGA1UEAxQgam9hby5taWtvc0BkZXBvc2l0LXNvbHV0
            aW9ucy5jb20wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC835vsduRe
            cvm8cTktiy7kRWRsOe381xvUXHkJElV+Xb9h4eUxWH2drekdZfaG0+jRRrT2yTY6
            agOsd5PJa6jimkypMJ7a75Hd22QcwhxiDz5VZC4kAFHiFCnSm8zKnzSr7qQ3icBP
            m5sXgF2M7XWjFlmDAX2nuEZHc+HleyqnTYTsygqDJsjh+u+ds87pyn6l8++5VPY4
            AefLVZpXTVAVMueHcO7cUuKW5CG/+fr7v2drZ/wl3vTYO+x+r1VeF4N0W7iiR3B4
            8i5700jvMnb91bmkvS+oFBQI+vpFgJuyY9AnftllGQM3vDmwRnjCjZXz7P+2v4GO
            fs08njM8sv3fN8lgNc4JgDpEM1SueVSIXIZ9E/gnl24wDW5xI8S95HwF+DXnYcRX
            CHVmYRbF5FFcARahxBuyOn3DSgS5pyJ53chjYbxHCvqMfbAo9vdbQkmck1CHQ3pK
            5nn1ZxpiwBiVco/aYBiEZTmQfU71DNEgUFCj4JZbBtKW7bggllmFlVYYYDXKYRgI
            n4CRgs947KyqGurD0Y5AVdRwFPx9mFOydkmfXkJbWwRmjt6Sd2uUo7SJCFXI+3ni
            GFzIKKd84EJKbmOFtHjvqqTM7nhwHgPKq//H0xtTMcnOW6LCuMimT63qEZjZYDxH
            bjCY7CUozRMBLu5qRMYzjBubXg3IRkbFXwIDAQABoAAwDQYJKoZIhvcNAQELBQAD
            ggIBAFMTYT+uyidGSuCI+9CmmMVvVr4PF4J9PGlWrZrSrfCR58tjPP/8mFHAtfLy
            /wf1jwZQgZOGzZp7VNaTT26uwZo/YoxrWmBWSQ5Md6WkAxnF1IqOUW5I61H/pTfh
            g83hKXNc/jQe9pCOxBMJ7ikGkHiGXDm2yL0YK0ym19DHS2Czh2YfcUMqS8sND6oa
            mPsv3/DQcRJyy0oCJ3VmqPlcZhjQTA1YQUf7xcwkiXDtWFovH41UUP2R3dVDseEo
            XN0sg56Kkmj/r+S3G+AcMnFwku5pGXSzfVwTjfCBJoTB7nhwOvJZ4qgMPtjpqdRD
            joZqVLyk8Xiivz9VEjJT9j7jGnCmYozBjn6QWW/8e0RxaOmZu9LnGr93SjkgAR2k
            fSwPPWJk4AdTBMwLDKobIR2I0V6cmCqvW+e+YsiaJbRqzjNvQs7voRqeHVdm6a/a
            dPz61g9mnEEVHmoXR0pmbmd4f9Z0a2nvtlVHBc1TGoqDxC5AEMfHTOIJcPFT34Kd
            cMQXeBAdPjSOYcF03BL7OXsDfK4/K8fP1XYZngD35+rMzjHqK1C+isP7V7KHf8aW
            R8NCCgji5q4HU8/MKwky1iWtFvPTSpLPrJ5w1vj7jgf4/mIEa00IZII/SOAFNpiv
            GCZpUu5EKiQVgzcjPvEJIVLQiP37HNSy9z6ioEGsIqLvx6kp
            -----END CERTIFICATE REQUEST-----

Client certificates with defined expiration dates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It might sometimes be necessary to issue a client certificate with a
fixed range when it is valid. To achieve this, the following 2 fields
must be specified under *options*:

-  not\_before: date when the certificate starts to be valid
-  not\_after: date when the certificate stops to be valid

Example:

.. code:: yaml

    pki:
      clients:
        firstname.lastname@external.com:
          csr: |
            -----BEGIN CERTIFICATE REQUEST-----
            MIIEcDCCAlgCAQAwKzEpMCcGA1UEAxQgam9hby5taWtvc0BkZXBvc2l0LXNvbHV0
            aW9ucy5jb20wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC835vsduRe
            cvm8cTktiy7kRWRsOe381xvUXHkJElV+Xb9h4eUxWH2drekdZfaG0+jRRrT2yTY6
            agOsd5PJa6jimkypMJ7a75Hd22QcwhxiDz5VZC4kAFHiFCnSm8zKnzSr7qQ3icBP
            m5sXgF2M7XWjFlmDAX2nuEZHc+HleyqnTYTsygqDJsjh+u+ds87pyn6l8++5VPY4
            AefLVZpXTVAVMueHcO7cUuKW5CG/+fr7v2drZ/wl3vTYO+x+r1VeF4N0W7iiR3B4
            8i5700jvMnb91bmkvS+oFBQI+vpFgJuyY9AnftllGQM3vDmwRnjCjZXz7P+2v4GO
            fs08njM8sv3fN8lgNc4JgDpEM1SueVSIXIZ9E/gnl24wDW5xI8S95HwF+DXnYcRX
            CHVmYRbF5FFcARahxBuyOn3DSgS5pyJ53chjYbxHCvqMfbAo9vdbQkmck1CHQ3pK
            5nn1ZxpiwBiVco/aYBiEZTmQfU71DNEgUFCj4JZbBtKW7bggllmFlVYYYDXKYRgI
            n4CRgs947KyqGurD0Y5AVdRwFPx9mFOydkmfXkJbWwRmjt6Sd2uUo7SJCFXI+3ni
            GFzIKKd84EJKbmOFtHjvqqTM7nhwHgPKq//H0xtTMcnOW6LCuMimT63qEZjZYDxH
            bjCY7CUozRMBLu5qRMYzjBubXg3IRkbFXwIDAQABoAAwDQYJKoZIhvcNAQELBQAD
            ggIBAFMTYT+uyidGSuCI+9CmmMVvVr4PF4J9PGlWrZrSrfCR58tjPP/8mFHAtfLy
            /wf1jwZQgZOGzZp7VNaTT26uwZo/YoxrWmBWSQ5Md6WkAxnF1IqOUW5I61H/pTfh
            g83hKXNc/jQe9pCOxBMJ7ikGkHiGXDm2yL0YK0ym19DHS2Czh2YfcUMqS8sND6oa
            mPsv3/DQcRJyy0oCJ3VmqPlcZhjQTA1YQUf7xcwkiXDtWFovH41UUP2R3dVDseEo
            XN0sg56Kkmj/r+S3G+AcMnFwku5pGXSzfVwTjfCBJoTB7nhwOvJZ4qgMPtjpqdRD
            joZqVLyk8Xiivz9VEjJT9j7jGnCmYozBjn6QWW/8e0RxaOmZu9LnGr93SjkgAR2k
            fSwPPWJk4AdTBMwLDKobIR2I0V6cmCqvW+e+YsiaJbRqzjNvQs7voRqeHVdm6a/a
            dPz61g9mnEEVHmoXR0pmbmd4f9Z0a2nvtlVHBc1TGoqDxC5AEMfHTOIJcPFT34Kd
            cMQXeBAdPjSOYcF03BL7OXsDfK4/K8fP1XYZngD35+rMzjHqK1C+isP7V7KHf8aW
            R8NCCgji5q4HU8/MKwky1iWtFvPTSpLPrJ5w1vj7jgf4/mIEa00IZII/SOAFNpiv
            GCZpUu5EKiQVgzcjPvEJIVLQiP37HNSy9z6ioEGsIqLvx6kp
            -----END CERTIFICATE REQUEST-----
          options:
            # Dates are always UTC
            not_before: '2017-12-08 15:00:00'
            not_after: '2017-12-21 21:30:00'

Client certificates renewal job
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``pki.clients`` state also deploys by default a job in charge of
periodically renewing the client certificates. The default schedule is
``5 8 * * *``, but it can be overridden with values under the
``pki:clients:cron`` pillar, as below:

.. code:: yaml

    pki:
      clients:
        # Renew the certificates every 30 minutes
        cron: '*/30 * * * *'

Managed server certificates
---------------------------

When using appliances it is a best practice to secure them with
certificates. Most appliances generate keys inside them and provide only
a CSR to be signed, which will required a server-class certificate to be
used.

These certificates can be issued against our *Intermediate CA* by using
the state ``pki.servers``.

These certificates are read from the pillar *pki:servers*. Each entry in
the pillar is a CN for the certificate. Under each entry, the following
fields are required:

-  csr: the actual CSR for the certificate issued.
-  attributes: options to be passed to the *x509.certificate\_managed*
   state.

**Managed server certificates will, by default, be valid for 365 days
and will be renewed 30 days before the expiration date**.

Managed server certificates will always be saved under
*/srv/pki/servers*.

An example pillar for a managed server certificate would be:

.. code:: yaml

    pki:
      servers:
        server1.prod.sec.network:
          policy: server-managed-blank
          attributes:
            subjectAltName:
              floating_host: DNS:server1.prod.sec.network
              actual_host: DNS:server1.prod.sec.network
              actual_ip: IP:10.18.5.80
          csr: |
            -----BEGIN CERTIFICATE REQUEST-----
            MIIE1zCCAr8CAQAwgZExJDAiBgNVBAMTG2luc2lnaHR2bTEucHJvZC5zZWMuZGVw
            b3NpdDEQMA4GA1UEBxMHSGFtYnVyZzEQMA4GA1UECBMHSGFtYnVyZzEXMBUGA1UE
            CxMOSW5mcmFzdHJ1Y3R1cmUxHzAdBgNVBAoTFkRlcG9zaXQgU29sdXRpb25zIEdt
            YkgxCzAJBgNVBAYTAkRFMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA
            v19byYNkPqeIFjU6rks97AqMg/sXBwE3E3OuxUWqiYLuvoFNZDHRqFV8zISRWllY
            8RprKzLDzkaHp2isH/Ih2EEQeD1o3sMDIsJozdXNtK8P1nbpWrDBq6Pio1rXiBIE
            PWDvoD8eOaTwThOFO0IM3TEvhwyMOgGzjMJoiUq/1ryvKlCrdxQ9y0xKZ0TKFnAi
            BE4ug5vSKiAHElK8AYcOTuLpuNT+QvzDP5ed2/JPaaXt8U2ul4uH0JrLtAoW1FB+
            vTasxKQGQW0Ohb2M1f1aAYpJwpajYhxBDIkDcFIbSdfE+CBGsr42g23elwyOhVn9
            PV2m/a523prtjVuFRN+yPNgRrZayr50gvgwoPXvO55wcDbrBIGGf4wJuw5jVH8nf
            oOe37q0Xttji40eec+GBhltgZRKEFz1XWS8qTVL2MEA9zidOOHBuG2KWeljRNOGt
            MsnV0WgZIqtUb4js91v3GKC6pRN4Csyp5NCQKmfKD3yUtVzFkjNgBi8RtZpGdlgz
            8p70J6aovGhSQHn7KtiPUk0jYoo9pz/MlexBxfevFoqk+vnfCL+XN0OrlTKC41QP
            TkjQ78e44xxoGp+BJCBfYS3ZP1ie2pEscmOvPeucIBwt9zElyBtACOWevo5Z1p0a
            wsyph+ahpsXK4UHRFPR8/cwtBU4LqRAF4rSO6VSe+I8CAwEAAaAAMA0GCSqGSIb3
            DQEBDQUAA4ICAQC+9ypIozhXNvqndZGQROEmiJphRalqt8eSaxIkJiBarAwET1+f
            ZunzxNAiRjIh6qbMl+3ASv3WWlhsF3LfT9udFoAsokOlW+/Vlj8rTCngqkLhRlL6
            bC4Bk8+GNU3psDPuAwWwWWmL9r1DGuDiJIDO1azzf340E6U4hu/nS3h2msf2GdFr
            ZHxCwKWAAlcJyVxPxkg2lYRXuxVg5TSLKcvc8zKbfLTTHA1DjLLrRz5/21eG3Eg4
            q89kbup2pLMHLZB2uG5jhhTJwayBNyXP0cBqe0bOdMqcrAySvAGwQWa6AyAyLsFL
            gc0sS9DEgeilVVC+b8UUK+ql1jqmRA/fPqATsELhA22/g+Ae7fSQ7074VG6YwUCa
            PjqyE5QSJIF0IRFmYEHGYNSglIoWS32TrJfyblSg6sGjvX3az2IuM/g+EPrzakLR
            wQcjCErtbjpZ8TaWWQn5DGNNr0NkooS704PAl3i0Q7zG+VM+lzKObq3tQD3pKtVm
            tPY1IsPvZhQatqVXH9QwQE50xbPl+yhQETE8QVwC12aSDdGAZrbJKceJkmVz3/tv
            NnJS1zt5mUY93VPvDiBZExNNh4cXGFtNNRcgiRzDurfzLVS6K2d6kj8gRXcK3g+S
            l77HQtZHqfPTIAzlepDOnFLR6QzfcTbv9PN+gdFyXSCrUBhP64sWC1i3JQ==
            -----END CERTIFICATE REQUEST-----

It is important for server certificates to contain all valid DNS
addresses and IPs in the subjectAltName. The appliance hostname should
be listed in the main hash entry and it will be used as the CN.

*Important*: The only policy available at this point is
``server-managed-blank``. This policy will not add any metadata to the
certificate (such as C, ST, L, O, and OU attributes). This requires the
CSR for the appliance to contain all of these attributes.

CRLs
----

Certificate Revocation Lists are managed with the ``pki.crl`` state.
This state requires the information to be available under the
``pki:crl`` pillar. Each authority should have an entry where the
storage path, CRL name, signing key, and signing certificate need to be
configured.

An example pillar for CSRs would go like:

.. code:: yaml

    pki:
      crl:
        rootca:
          path: /srv/crl
          name: root.crl.pem
          private_key: /srv/pki/rootca.key
          signing_cert: /srv/pki/rootca.pem
          digest: sha512
          #revoked: commented out if there is nothing to be revoked
        intermediateca:
          path: /srv/crl
          name: intermediate.crl.pem
          private_key: /srv/pki/intermediateca.key
          signing_cert: /srv/pki/intermediateca.pem
          digest: sha512
          revoked:
            old.employee@company.com:
              serial_number: 04:C8:F5:D5:51:96:A7:E0
              revocation_date: '2019-04-17 00:00:00'
              reason: cessationOfOperation
            another.old.employee@company.com:
              serial_number: DE:6F:97:54:19:81:CE:BB
              revocation_date: '2019-04-18 00:00:00'
              reason: cessationOfOperation

